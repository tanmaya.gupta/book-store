json.extract! book, :id, :name, :authors, :price, :description, :ratings, :created_at, :updated_at
json.url book_url(book, format: :json)
