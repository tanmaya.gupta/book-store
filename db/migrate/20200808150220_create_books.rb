class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :name
      t.string :authors
      t.decimal :price
      t.text :description
      t.decimal :ratings

      t.timestamps
    end
  end
end
